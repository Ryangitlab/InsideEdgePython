"""Main script for generating output.csv."""
import pandas as pd
import numpy as np
import csv

fname_data = './data/raw/pitchdata.csv'
fname_splits = './data/reference/combinations.txt'
columns = ['SubjectID','Stat','Split','Subject','Value']
PA_cutoff = 25
stat_names = ['H','AB','BB','HBP','TB','SF']


def get_stat_sums(table,stat_names):
    stat_sums={}
    for stat_name in stat_names:
        stat_sums[stat_name]=table[stat_name].sum()
    return stat_sums

def split_to_hand_pos(split):
    hand_pos = split.split(' ')[1]
    return hand_pos[0],hand_pos[2]


def AVG(stat_sums):
    if stat_sums['AB'] == 0:
        return 0
    else:
        return float(stat_sums['H'])/float(stat_sums['AB'])


def OBP(stat_sums):
    if stat_sums['AB']+stat_sums['BB']+stat_sums['HBP']+stat_sums['SF'] == 0:
        return 0
    else:
        return float(stat_sums['H']+stat_sums['BB']+stat_sums['HBP'])/float(stat_sums['AB']+stat_sums['BB']+stat_sums['HBP']+stat_sums['SF'])

def SLG(stat_sums):
    if stat_sums['AB'] == 0:
        return 0
    else:
        return float(stat_sums['TB'])/float(stat_sums['AB'])


def OPS(stat_sums):
    return OBP(stat_sums)+SLG(stat_sums)

def main():
    data = pd.read_table(fname_data,sep=',') #read in data
    combinations = pd.read_table(fname_splits,sep=',') #read in combinations

    stats = list(set(combinations.loc[:,'Stat']))
    subjects = list(set(combinations.loc[:,'Subject']))
    splits = list(set(combinations.loc[:,'Split']))

    master_splits = [[] for i in range(len(data))] #create an array to hold splits
    globals_dict = globals()
    index = 0

    #loop through each split
    for split in splits: 
        #parse split to determine player position and handedness of opposition
        hand,pos = split_to_hand_pos(split) 
        #find events that satisfy split qualifications
        if pos == 'P':
            split_subsection = data[getattr(data,'PitcherSide')==hand]
        else:
            split_subsection = data[getattr(data,'HitterSide')==hand]
        #loop through each applicable subject 
        for subject in subjects:
            if ('Hitter' in subject and pos=='H') or ('Pitcher' in subject and pos=='P'):
                continue
            #loop through each player that meets split qualifications
            for subjectID in list(set(split_subsection.loc[:,subject])):
                #acquire all events in which subject equals subjectID
                split_player = split_subsection[getattr(split_subsection,subject)==subjectID]

                #check if player has atleast 25 plate appearances
                PA_dict = get_stat_sums(split_player,['PA'])
                if PA_dict['PA']<PA_cutoff:
                    continue
                #if they do, get the rest of their stats
                stat_sums = get_stat_sums(split_player,stat_names)
                stat_sums.update(PA_dict)

                #calculate stat values, round to 3 decimals and assign them to the master list
                for stat in stats:
                    stat_function=globals_dict[stat]
                    value = round(stat_function(stat_sums),3)
                    master_splits[index] = [subjectID,stat,split,subject,value]
                    index+=1

    #cut off master list at first empty index
    master_splits = master_splits[:index]
    #sort columns in ascending order
    master_splits.sort()

    #write to file
    dialect=csv.excel
    dialect.lineterminator='\n'
    with open("./data/processed/output.csv", "wb") as f:
        writer = csv.writer(f,dialect=dialect)
        writer.writerow(columns)
        writer.writerows(master_splits)


if __name__ == '__main__':
    main()

